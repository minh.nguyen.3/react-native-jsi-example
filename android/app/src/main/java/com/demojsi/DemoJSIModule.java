package com.demojsi;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class DemoJSIModule extends ReactContextBaseJavaModule {

    static {
        System.loadLibrary("md5jsi");
    }

    private static native void initialize(long jsiPtr);
    private static native void destruct();


    @Override
    public void initialize() {
        super.initialize();
        if (context != null) {
            DemoJSIModule.initialize(context.getJavaScriptContextHolder().get());
        }
    }

    @Override
    public void onCatalystInstanceDestroy() {
        DemoJSIModule.destruct();
    }

    public ReactApplicationContext context;
    public DemoJSIModule(ReactApplicationContext reactContext) {
        this.context = reactContext;
    }

    @NonNull
    @NotNull
    @Override
    public String getName() {
        return "DemoJSIModule";
    }
}
