#include <jni.h>
#include "jsi-module.h"

extern "C"
JNIEXPORT void JNICALL
Java_com_demojsi_DemoJSIModule_initialize(JNIEnv* env, jclass clazz, jlong jsiPtr) {
  if (!jsiPtr) return;
  installJSIModule(*reinterpret_cast<facebook::jsi::Runtime*>(jsiPtr));
}

extern "C"
JNIEXPORT void JNICALL
Java_com_demojsi_DemoJSIModule_destruct(JNIEnv* env, jclass clazz) {
  cleanupJSI();
}