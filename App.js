/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  NativeModules,
  View,
} from 'react-native';

import {Header} from 'react-native/Libraries/NewAppScreen';

const {DemoModule} = NativeModules;

const json100mb = require('./src/100mb.json');
const jsonFile = require('./src/demo.json');

const App = () => {
  const [data, setData] = useState('');
  const [md5, setMd5] = useState('');
  const [md5jsi, setMd5JSI] = useState('');

  const [sum, setSum] = useState(0);
  const getData = () => {
    const jsonString = JSON.stringify(json100mb);
    console.time('load data');
    DemoModule.getData(jsonString, newData => {
      console.timeEnd('load data');
      const strLength = newData.length;
      setData(`New data: \n${newData.substring(strLength - 12, strLength)}`);
    });
  };

  const getBigData = () => {
    const jsonString = JSON.stringify(json100mb);
    /// jsi
    console.time('load big data');
    const newData = global.getBigData(jsonString);
    console.timeEnd('load big data');
    const strLength = newData.length;
    setData(`New data: \n${newData.substring(strLength - 12, strLength)}`);
  };

  const hashMd5 = () => {
    const str = JSON.stringify(jsonFile);
    console.time('hash md5');
    DemoModule.hashMd5(str, md5 => {
      console.timeEnd('hash md5');
      console.log('md5 string: ', md5);
      setMd5(md5);
    });
  };

  const hashMd5JSI = () => {
    const str = JSON.stringify(jsonFile);
    console.time('hash md5jsi');
    const hash = global.hashMd5jsi(str);
    console.timeEnd('hash md5jsi');
    setMd5JSI(hash);
  };

  const sum2number = () => {
    const sum = global.sum2number(2, 3);
    setSum(sum);
  };

  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <Header />
        <TouchableOpacity style={{padding: 20}} onPress={getData}>
          <Text>get string</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{padding: 20}} onPress={getBigData}>
          <Text>get big string</Text>
        </TouchableOpacity>
        <Text>Result: {data}</Text>
        <TouchableOpacity style={{padding: 20}} onPress={hashMd5}>
          <Text>hash md5</Text>
        </TouchableOpacity>
        <Text>Result: {md5}</Text>
        <TouchableOpacity style={{padding: 20}} onPress={hashMd5JSI}>
          <Text>hash md5 jsi</Text>
        </TouchableOpacity>
        <Text>Result: {md5jsi}</Text>
        <TouchableOpacity style={{padding: 20}} onPress={sum2number}>
          <Text>sum 2 number</Text>
        </TouchableOpacity>
        <Text>Result: {sum}</Text>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
