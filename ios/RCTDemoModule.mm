//
//  RCTDemoModule.m
//  demojsi
//
//  Created by Minh Nguyễn on 19/11/2021.
//

#import <Foundation/Foundation.h>
#import "RCTDemoModule.h"
#import <React/RCTBridge+Private.h>
#import <React/RCTUtils.h>
#import <CommonCrypto/CommonCrypto.h>

@implementation DemoModule


@synthesize bridge = _bridge;
@synthesize methodQueue = _methodQueue;


RCT_EXPORT_MODULE();


+ (BOOL)requiresMainQueueSetup {
  return YES;
}

- (void)setBridge:(RCTBridge *)bridge
{
  _bridge = bridge;
  _setBridgeOnMainQueue = RCTIsMainQueue();
  
  [self setup];
}

- (void)setup {
  RCTCxxBridge *cxxBridge = (RCTCxxBridge *)self.bridge;
  if (!cxxBridge.runtime) {
    // retry 10ms later - THIS IS A WACK WORKAROUND. wait for TurboModules to land.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.001 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
      [self setup];
    });
    return;
  }
  
  installJSIModule(*(facebook::jsi::Runtime *)cxxBridge.runtime);
}

- (void)invalidate {
  cleanupJSI();
}


RCT_EXPORT_METHOD(getData: (NSString*)input callback:(RCTResponseSenderBlock)callback) {
  NSString* newString = [NSString stringWithFormat:@"%@%@", input, @"\nadd them ne!"];
  callback(@[newString]);
}

RCT_EXPORT_METHOD(hashMd5: (NSString*)s callback:(RCTResponseSenderBlock)callback) {
  const char* cstr = [s UTF8String];
  unsigned char digest[CC_MD5_DIGEST_LENGTH];
  CC_MD5(cstr, (CC_LONG)strlen(cstr), digest);
  
  NSMutableString* output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
  for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
    [output appendFormat:@"%02x", digest[i]];
  }
  
  callback(@[output]);
}


@end
