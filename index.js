/**
 * @format
 */
// in your root javascript file
import 'react-native-console-time-polyfill';

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import MessageQueue from 'react-native/Libraries/BatchedBridge/MessageQueue.js';

const spyFunction = msg => {
  // native to javascript
  if (msg?.type === 0) {
    console.log('bridge method: ', msg?.method);
  }
};

MessageQueue.spy(spyFunction);

AppRegistry.registerComponent(appName, () => App);
