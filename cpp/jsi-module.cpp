#import "jsi-module.h"
#import "md5.h"
#include <iostream>
#include <sstream>

using namespace facebook;

// Returns false if the passed value is not a string or an ArrayBuffer.
bool md5_valueToString(jsi::Runtime& runtime, const jsi::Value& value, std::string* str) {
  if (value.isString()) {
    *str = value.asString(runtime).utf8(runtime);
    return true;
  }

  if (value.isObject()) {
    auto obj = value.asObject(runtime);
    if (!obj.isArrayBuffer(runtime)) {
      return false;
    }
    auto buf = obj.getArrayBuffer(runtime);
    *str = std::string((char*)buf.data(runtime), buf.size(runtime));
    return true;
  }

  return false;
}

void installJSIModule(jsi::Runtime& jsiRuntime) {
  std::cout << "Initializing JSIModule" << "\n";

  // MARK: HostObject function
  // declare a host function
  auto hashMd5jsi = jsi::Function::createFromHostFunction(
      jsiRuntime,
      jsi::PropNameID::forAscii(jsiRuntime, "hashMd5jsi"),
      1,  // string or ArrayBuffer
      [](jsi::Runtime& runtime, const jsi::Value& thisValue, const jsi::Value* arguments, size_t count) -> jsi::Value {
        // declare
        std::string str;
        if(!md5_valueToString(runtime, arguments[0], &str)) {
          return jsi::Value(-1);
        }
        std::string strMd5 = md5(str);

        return jsi::Value(jsi::String::createFromUtf8(runtime, strMd5));
      }
  );
  // register a host function to javascript runtime
  jsiRuntime.global().setProperty(jsiRuntime, "hashMd5jsi", std::move(hashMd5jsi));

  
  // MARK: HostObject function
  auto sum2number = jsi::Function::createFromHostFunction(
          jsiRuntime,
          jsi::PropNameID::forUtf8(jsiRuntime, "sum2number"),
          2,
          [](jsi::Runtime& runtime, const jsi::Value& thisValue, const jsi::Value* arguments, size_t count) -> jsi::Value {

              int number1 = arguments[0].getNumber();
              int number2 = arguments[1].getNumber();

              auto sum = number1 + number2;

              return jsi::Value(sum);

  });
  jsiRuntime.global().setProperty(jsiRuntime, "sum2number", std::move(sum2number));

  // MARK: HostObject function
  auto getBigData = jsi::Function::createFromHostFunction(
          jsiRuntime,
          jsi::PropNameID::forUtf8(jsiRuntime, "getBigData"),
          2,
          [](jsi::Runtime& runtime, const jsi::Value& thisValue, const jsi::Value* arguments, size_t count) -> jsi::Value {

              std::string string = arguments[0].asString(runtime).utf8(runtime);

              string.append("\nadd them ne!");

              return jsi::String::createFromUtf8(runtime, string);
          });
  jsiRuntime.global().setProperty(jsiRuntime, "getBigData", std::move(getBigData));

}

void cleanupJSI() {
  std::cout << "Cleanup JSIModule" << "\n";
}
